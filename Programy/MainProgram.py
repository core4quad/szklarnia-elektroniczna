from ArduinoCommunicationTest import *

print "wykryto arduino"

##########################################################################
##
##      Funkcja do komunikacji z arduino:
##      x=send("WIADOMOSC")
##      Zawsze w postaci zmienna=send(WIADOMOSC)
##
##      Mozliwe wiadomosci:
##
##      TEMPERATURE3?
##      zwraca temperature ze sloika nr3 (w stopniach celcjusza np: 35 oznacza 35 stopni celcjusza)
##      mozliwe sloiki: 1,2,3,4
##
##      HUMIDITY3?
##      zwraca wilgotnosc ze sloika nr3 (w procentach, np: 35 oznacza 35%)
##      mozliwe sloiki: 1,2,3,4
##
##      FOT3?
##      zwraca naslonecznienie sloika nr3 (wartosci od 0 do 1024)
##      mozliwe sloiki: 1,2,3,4
##
##      LED2ON
##      wlacza lampke w sloiku nr2 
##      mozliwe sloiki: 1,2
##
##      LED2OFF
##      wylacza lampke w sloiku nr2
##      mozliwe sloiki: 1,2
##
##################################################################################
##
##Przyklad:
print "Wilgotnosc w sloiku nr3:"
x=send("HUMIDITY3?") #Pytamy sie o wilgotnosc w sloiku nr3
print x #Wyswietli sie wilgotnosc w sloiku nr3 czyli 55 :)
print " "
print "Wylaczenie LED2:"
x=send("LED2OFF?") #Mowimy arduino by wylaczyl lampke nr2
print "Wylaczono LED2"

######################################################################################

##Dalej wasz graficzny program :) gosciu chce by bylo to robione w Pythonie3 a za okienka ma odpowiadac tkinter.
