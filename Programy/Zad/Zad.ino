#include "DHT.h"
#define DHTTYPE DHT11  //Uzywamy czujnika DHT11
#define DHTPIN1 9  //pierwszy czujnik DHT
#define DHTPIN2 10  //drugi czujnik DHT
DHT dht1(DHTPIN1, DHTTYPE); //pierwszy czujnik
DHT dht2(DHTPIN2, DHTTYPE); //drugi czujnik

String napis; //potrzebne do komunikacji RS232
char litera; //potrzebne do komunikacji RS232

#define LED 2 //port diody LED

char LED_stan=HIGH;

#define FOT1 A0//port pierwszego czujnika swiatla
#define FOT2 A1//port drugiego czujnika swiatla


void setup()
{
  pinMode(8,OUTPUT); //Kontrolka LED
  digitalWrite(8,HIGH);
  
  Serial.begin(9600);
  dht1.begin();
  dht2.begin();
  pinMode(LED,OUTPUT);
  
  pinMode(FOT1,INPUT);
  digitalWrite(FOT1,LOW);
  pinMode(FOT2,INPUT);
  digitalWrite(FOT2,LOW);
}

void loop()
{ 
  digitalWrite(LED,LED_stan);
  
  if (Serial.available() > 0) //Dziala tylko gdy nawiazano polaczenie
  {
    litera=Serial.read();
    if (litera!='#')
    {napis+=litera;}
    else
    {
      if (napis==("COM?"))
        {Serial.println("OK");}
      else if (napis=="TEMPERATURE1?")
         {
            float t = dht1.readTemperature();
            Serial.println(t);
         }
      else if (napis=="TEMPERATURE2?")
         {
            float t = dht2.readTemperature();
            Serial.println(t);
         }
      else if (napis=="HUMIDITY1?")
         {
            float h = dht1.readHumidity();
            Serial.println(h);
         }
      else if (napis=="HUMIDITY2?")
         {
            float h = dht2.readHumidity();
            Serial.println(h);
         }
      else if (napis=="FOT1?")
         {
            int f=analogRead(FOT1);
            Serial.println(f);
         }
      else if (napis=="FOT2?")
         {
            int f=analogRead(FOT2);
            Serial.println(f);
         }
      else if (napis=="LEDON?")
         {LED_stan=LOW;}
      else if (napis=="LEDOFF?")
         {LED_stan=HIGH;}
         
      napis="";
    }
  }
}
