import time
import serial


def wake_up(connection):
    connection.write("P")
    time.sleep(1)
    temp=connection.readline()
    time.sleep(1)
    return None


def initialize():
    for i in range(1,10):
        try:
            connection=serial.Serial("COM"+str(i), 9600,timeout=1.,write_timeout=1.)
            temp1=wake_up(connection)
            connection.write("#COM?#")
            time.sleep(1)
            ret=False
            ret=connection.readline()
            time.sleep(1)
            connection.close()
            if ret[:-2]=="OK":
                return i
        except:
            pass
    return False

port=initialize()
print port
    
def send(message):
    """message:STRING"""
    try:
        connection=serial.Serial("COM"+str(port), 9600,timeout=1.,write_timeout=1.)
        temp1=wake_up(connection)
        connection.write("#"+str(message)+"#")
        time.sleep(1)
        ret=False
        ret=connection.readline()
        time.sleep(1)
        connection.close()
    except:
        return False
    return ret[:-2]
