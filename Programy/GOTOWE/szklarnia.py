# -*- coding: utf-8 -*-

#from ArduinoCommunication import *
import ArduinoCommunication as AC
#import Tkinter.messagebox as tkm
import sys
if sys.hexversion > 0x03000000:
    import tkinter as tk
    import tkinter.messagebox as tkmb
else:
    import Tkinter as tk
    import tkMessageBox as tkmb

class clsSzklarnia(tk.Tk):
    def __init__(self, parent=None):
        tk.Tk.__init__(self, parent)
        self.parent = parent
        self.grid()
        self.robekran()
        pass

    def robekran(self):
        self.thelabel=tk.Label(self, text="Witaj w szklarni! \n Którą hodowlą chcesz sie zajac? \n Aby wyjsc kliknij 'Zamknij'")
        self.thelabel.grid(row=0, column=0, columnspan=3)
        self.b_ex=tk.Button(self,text='Zamknij', command=self.ex, fg='red')
        self.b_ex.grid(row=0, column=3, sticky = tk.N+tk.S+tk.W+tk.E)
        self.b_h1=tk.Button(self, text='  Hodowla 1', fg='green')
        self.b_h2=tk.Button(self, text='  Hodowla 2', fg='green', command=self.cmdHod2)
        self.b_h3=tk.Button(self, text='  Hodowla 3', fg='green', command=self.cmdHod3)
        self.b_h4=tk.Button(self, text='  Hodowla 4', fg='green')        
        self.b_h1.grid(row=1, column = 0, sticky = tk.N+tk.S+tk.W+tk.E)
        self.b_h2.grid(row=1, column = 1, sticky = tk.N+tk.S+tk.W+tk.E)
        self.b_h3.grid(row=1, column = 2, sticky = tk.N+tk.S+tk.W+tk.E)
        self.b_h4.grid(row=1, column = 3, sticky = tk.N+tk.S+tk.W+tk.E)
        self.lblHodowla=tk.Label(self,text='Hodowla X:')
        self.lblHodowla.grid(column=1, row =2, columnspan=2, sticky = tk.N+tk.S+tk.W+tk.E)
        
        self.lblTemp=tk.Label(self, text='Temperatura:', bg='red')
        self.lblTemp.grid(column = 0, row=3, sticky = tk.W+tk.E)
        self.lblTemp_wart=tk.Label(self, text='XXX')
        self.lblTemp_wart.grid(column = 3, row=3, sticky = tk.E)
        
        self.lblWilg=tk.Label(self, text='Wilgotnosc:', bg='blue')
        self.lblWilg.grid(column = 0, row=4, sticky = tk.W+tk.E)
        self.lblWilg_wart=tk.Label(self, text='XXX')
        self.lblWilg_wart.grid(column = 3, row=4, sticky = tk.E)
        
        self.lblNasl=tk.Label(self, text='Naslonecznienie:', bg='orange')
        self.lblNasl.grid(column=0,row=5)
        self.lblNasl_wart=tk.Label(self, text='XXX')
        self.lblNasl_wart.grid(column = 3, row=5, sticky = tk.E)

    def cmdHod2(self):
        self.lblHodowla["text"] = "Hodowla 2:"
        self.lblTemp_wart["text"] = str(self.sczytaj("#TEMPERATURE1?#"))
        self.lblWilg_wart["text"] = str(self.sczytaj("#HUMIDITY1?#"))
        self.lblNasl_wart["text"] = str(float(self.sczytaj("#FOT1?#"))/1024.),"%"#ZAOKRĄGLIĆ        
        if tkmb.askyesnocancel("Lampka LED","Włączyć lampkę LED?"):
            self.sczytaj("#LEDON?#")
        else:
            self.sczytaj("#LEDOFF?#")
            
    def cmdHod3(self):
        self.lblHodowla["text"] = "Hodowla 3:"
        self.lblTemp_wart["text"] = str(self.sczytaj("#TEMPERATURE2?#")), 'st. C'
        self.lblWilg_wart["text"] = str(self.sczytaj("#HUMIDITY2?#")), "%"
        self.lblNasl_wart["text"] = str(float(self.sczytaj("#FOT2?#"))/1024.),"%"#ZAOKRĄGLIĆ
        pass

    def sczytaj(self,x):#sczytywanie z arduino
        self.odpowiedz=AC.send(x)
        print (self.odpowiedz)
        print type(x)
        print ("odczytuje: "+str(x))
        return self.odpowiedz
        
    
    def ex(self):
        #MB.showinfo("allalalala","wadadaad")
        #MB.showerror("allalalala","wadadaad")
        #MB.showwarning("allalalala","wadadaad")
        self.destroy()
        #quit()


print "wykryto arduino"


if __name__ == "__main__":
    app = clsSzklarnia(None)
    app.title("Szklarnia")
    app.mainloop()
